# Parses sway tree and extracts focused pid.
# Then it descends into the most recent pid and returns its cwd.
# I am using it for getting cwd for a new terminal - like Gnome terminal's Ctrl-N.

# tl;dr - exec alacritty => exec alacritty --working-directory "$(swaypwd)"

import json, osproc, lists, strformat, strutils, sequtils, sugar, options
import os, strutils, shells

type
    Pid = distinct int

    SwayNodeKind = enum
        App
        Con

    SwayNode = ref object
        focused: bool
        case kind: SwayNodeKind
            of App:
                pid: Pid
            of Con:
                nodes: seq[SwayNode]


proc newPid(pid: int): Pid = Pid(pid)
proc `$`*(pid: Pid): string {. borrow .}
proc `<`*(a: Pid, b: Pid): bool {. borrow .}
proc strip0*(str: string): string =
    if str.len > 0 and str[str.len - 1] == '\x00':
        str.substr(0, str.len-2)#.strip0() #urge.... to... recurse!
    else:
        str

## for some reason it reads cmdline with \x00 in the end .-.
proc cmdline*(pid: Pid): string = expandSymlink(fmt"/proc/{pid}/exe")
proc children*(pid: Pid): seq[Pid] = readFile(fmt"/proc/{pid}/task/{pid}/children").strip0().strip().split(" ").filterIt(it != "").map(parseInt).map(newPid)
proc cwd*(pid: Pid): string = expandSymlink(fmt"/proc/{pid}/cwd")


proc headOrNil*[T](s: seq[T]): Option[T] =
    try:
        s[0].some
    except IndexError:
        none(T)


proc parseSwayTreeNode(node: JsonNode): SwayNode =
    if node.contains("pid"):
        # program with a pid
        SwayNode(
            kind: App,
            pid: node["pid"].to(Pid),
            focused: node["focused"].to(bool)
        )
    elif node.contains("nodes"):
        # container with nodes
        SwayNode(
            kind: Con,
            focused: node["focused"].to(bool),
            nodes: node["nodes"].getElems().map(parseSwayTreeNode)
        )
    else:
        # IDK tf is that. usually scratchpad, but whatever
        SwayNode(kind: Con, nodes: @[])


proc findNode*(node: SwayNode, pred: (SwayNode) -> bool): Option[SwayNode] =
    ##
    ## Searches recursively for a node in sway tree. Returns nil if not found
    ##
    if pred node:
        some(node)
    else:
        case node.kind
            of Con:
                node.nodes
                .mapIt(it.findNode(pred))
                .filterIt(it.isSome)
                .mapIt(it.get(nil))
                .headOrNil()
            of App:
                none(SwayNode)


proc getSwayTree(): SwayNode =
    ##
    ## Returns sway window tree
    ##
    let json = parseJson osproc.execProcess("swaymsg -t get_tree")
    parseSwayTreeNode(json)


proc getBottomShell(pid: Pid, shells: seq[string]): Pid =
    ##
    ## Finds the most recent child matching one of the shell names
    ## Then does the same with it's children, until process without subshells found
    ## Does not work for some cases, (e.g. VSCode), when parent is not direct a ancestor.
    ##
    let children = pid.children()
        .filter((pid) => shells.anyIt(pid.cmdline.lastPathPart == it))
    stderr.writeLine " [", pid, "] ", pid.cmdline.lastPathPart, " at ", pid.cwd, " -> ", children
    if len(children) == 0:
        pid
    else:
        getBottomShell(children.max(), shells)

try:

    let nodeO = getSwayTree().findNode((a) => a.focused)
    let node = if nodeO.isSome():
        nodeO.get()
    else:
        stderr.writeLine "No focused windows, perhaps you are in an overlay!"
        echo getEnv("HOME")
        quit(0)

    case node.kind
        of App:
            echo getBottomShell(node.pid, enabledShells).cwd
        of Con:
            echo getEnv("HOME")
except json.JsonParsingError as e:
    stderr.writeLine "Error, json cannot be parsed!"
    echo getEnv("HOME")
    quit(1)
