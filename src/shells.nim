# Add your terminal emulator here.
const enabledShells: seq[string] = @[ "bash", "zsh", "fish", "sh", "posh", "codium" ]
export enabledShells
