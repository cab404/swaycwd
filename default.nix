{ nimPackages
, enableShells ? [ "bash" "zsh" "fish" "sh" "posh" ]
}: nimPackages.buildNimPackage {
  pname = "swaycwd";
  version = "0.1.0";
  src = ./.;
  nimFlags = [ "--opt:speed" ];
  preConfigure = ''
    {
      echo 'let enabledShells: seq[string] = @${builtins.toJSON enableShells}'
      echo 'export enabledShells'
    } > src/shells.nim
  '';
}
